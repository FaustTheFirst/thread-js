import { PostReactionModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class PostReactionRepository extends BaseRepository {
  getPostReaction(userId, postId) {
    return this.model.findOne({
      group: [
        'postReaction.id',
        'user.id',
        'user->image.id'
      ],
      where: { userId, postId },
      attributes: ['id', 'isLike'],
      include: {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }
    });
  }
}

export default new PostReactionRepository(PostReactionModel);
