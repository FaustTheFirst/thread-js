import { CommentReactionModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentReactionRepository extends BaseRepository {
  getCommentReaction(userId, commentId) {
    return this.model.findOne({
      group: [
        'commentReaction.id',
        'user.id',
        'user->image.id'
      ],
      where: { userId, commentId },
      attributes: ['id', 'isLike'],
      include: {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }
    });
  }
}

export default new CommentReactionRepository(CommentReactionModel);
