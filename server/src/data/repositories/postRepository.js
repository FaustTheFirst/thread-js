import { Op } from 'sequelize';
import sequelize from '../db/connection';
import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel
} from '../models/index';
import BaseRepository from './baseRepository';

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      showUserPosts,
      showLikedPosts,
      showDislikedPosts
    } = filter;

    const where = {};
    let likesFilter;

    if (showUserPosts === 'true') {
      Object.assign(where, { userId });
    } else if (showUserPosts === 'false') {
      Object.assign(where, { userId: { [Op.ne]: userId } });
    }

    if (showLikedPosts === 'true' || showDislikedPosts === 'true') {
      likesFilter = `(SELECT "postId" FROM "postReactions" WHERE "postReactions"."userId" = '${userId}'`;

      if (showLikedPosts !== showDislikedPosts) {
        likesFilter += `AND "postReactions"."isLike" IS ${showLikedPosts === 'true' ? 'TRUE' : 'FALSE'})`;
      } else {
        likesFilter += ')';
      }

      Object.assign(where, {
        id: {
          [Op.in]: sequelize.literal(likesFilter)
        }
      });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`),
          'commentCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        attributes: ['id', 'isLike'],
        duplicating: false,
        include: {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id',
        'postReactions.id',
        'postReactions->user.id',
        'postReactions->user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'comments->commentReactions.id',
        'comments->commentReactions->user.id',
        'comments->commentReactions->user->image.id',
        'user.id',
        'user->image.id',
        'image.id',
        'postReactions.id',
        'postReactions->user.id',
        'postReactions->user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(`
                        (SELECT COUNT(*)
                        FROM "comments" as "comment"
                        WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" IS NULL)`),
          'commentCount']
        ]
      },
      include: [{
        model: CommentModel,
        include: [{
          model: UserModel,
          attributes: ['id', 'username', 'userStatus'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }, {
          model: CommentReactionModel,
          attributes: ['id', 'isLike'],
          duplicating: false,
          include: {
            model: UserModel,
            attributes: ['id', 'username'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          }
        }]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: ['id', 'isLike'],
        include: {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      }]
    });
  }
}

export default new PostRepository(PostModel);
