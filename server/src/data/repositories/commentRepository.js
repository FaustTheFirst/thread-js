import { CommentModel, UserModel, ImageModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

class CommentRepository extends BaseRepository {
  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id',
        'commentReactions.id',
        'commentReactions->user.id',
        'commentReactions->user->image.id'
      ],
      where: { id },
      include: [{
        model: UserModel,
        attributes: ['id', 'username', 'userStatus'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: CommentReactionModel,
        attributes: ['id', 'isLike'],
        include: {
          model: UserModel,
          attributes: ['id', 'username'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        }
      }]
    });
  }
}

export default new CommentRepository(CommentModel);
