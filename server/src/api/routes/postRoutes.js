import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => {
      if (post) {
        res.send(post);
      } else {
        req.io.to(req.user.id).emit('deleted_post', 'Seems like that post was deleted');
        res.send({ status: 404 });
      }
    })
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        if (reaction.isLike) {
          req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
        } else {
          req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked...');
        }
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req, res, next) => postService.update(req.params.id, req.body)
    .then(post => res.send(post))
    .catch(next))
  .delete('/:id', (req, res, next) => postService.deletion(req.params.id)
    .then(post => {
      res.send({ deleteStatus: `${post}` });
    })
    .catch(next));

export default router;
