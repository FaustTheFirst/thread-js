import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, imageId, image, userStatus } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image, userStatus };
};

export const updateUserStatus = async (userId, status) => {
  const { id } = await userRepository.updateById(userId, status);
  const { username, email, imageId, image, userStatus } = await userRepository.getUserById(id);
  return { id, username, email, imageId, image, userStatus };
};
