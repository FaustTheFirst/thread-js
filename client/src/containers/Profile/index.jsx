import React, { useState, useRef, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Grid, Image, Input, Button, Icon } from 'semantic-ui-react';
import { updateUserStatus } from './actions';

const Profile = ({ user, updateUserStatus: updateStatus }) => {
  const [isEditStatus, setIsEditStatus] = useState(false);
  const [status, setStatus] = useState(user.userStatus);
  const inputRef = useRef(null);

  useEffect(() => {
    if (isEditStatus) {
      inputRef.current.focus();
    }
  }, [isEditStatus]);

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image centered src={getUserImgLink(user.image)} size="medium" circular />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled
          value={user.username}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        <Input
          icon="chat"
          iconPosition="left"
          placeholder="Status"
          type="text"
          value={status || ''}
          disabled={!isEditStatus}
          ref={inputRef}
          onChange={ev => setStatus(ev.target.value)}
        />
        <br />
        {!isEditStatus
          ? (
            <Button
              color="blue"
              content="Edit status"
              onClick={() => {
                setIsEditStatus(!isEditStatus);
                inputRef.current.focus();
              }}
            />
          )
          : (
            <>
              <Button
                icon
                color="green"
                onClick={() => {
                  updateStatus(status);
                  setIsEditStatus(!isEditStatus);
                }}
              >
                <Icon name="check" />
              </Button>
              <Button
                icon
                color="yellow"
                onClick={() => {
                  setStatus(user.userStatus);
                  setIsEditStatus(!isEditStatus);
                }}
              >
                <Icon name="ban" />
              </Button>
            </>
          )}
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateUserStatus: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = { updateUserStatus };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
