import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Form } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import EditButtons from 'src/components/EditButtons';
import ReactionButtons from 'src/components/ReactionButtons';
import { reactComment, updateComment } from 'src/containers/Thread/commentActions';

import styles from './styles.module.scss';

const Comment = ({
  comment,
  activeUser,
  reactComment: setCommentReaction,
  updateComment: setCommentUpdation,
  deleteModal
}) => {
  const {
    id,
    body,
    user,
    createdAt,
    updatedAt,
    commentReactions
  } = comment;
  const [isEditComment, setIsEditComment] = useState(false);
  const [commentField, setCommentField] = useState(body);
  const commentHolder = activeUser === user.id;

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
          {createdAt !== updatedAt ? ` (updated ${moment(updatedAt).fromNow()})` : null}
        </CommentUI.Metadata>
        <br />
        <CommentUI.Metadata>
          {user.userStatus}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {commentHolder
          && isEditComment
            ? (
              <Form>
                <Form.TextArea
                  error={commentField.length === 0}
                  value={commentField}
                  onChange={ev => setCommentField(ev.target.value)}
                />
              </Form>
            )
            : body}
        </CommentUI.Text>
      </CommentUI.Content>
      <CommentUI.Content>
        <ReactionButtons
          id={id}
          activeUser={activeUser}
          arrayOfReactions={commentReactions}
          setReaction={setCommentReaction}
        />
        {commentHolder
          ? (
            <EditButtons
              id={id}
              isEdit={isEditComment}
              setIsEdit={() => setIsEditComment(!isEditComment)}
              editField={commentField}
              setEditField={() => setCommentField(body)}
              setUpdation={setCommentUpdation}
              deleteModal={deleteModal}
              modalCaller="comment"
            />
          )
          : null}
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  activeUser: PropTypes.string,
  reactComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteModal: PropTypes.func
};

Comment.defaultProps = {
  activeUser: undefined,
  deleteModal: undefined
};

const actions = { reactComment, updateComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(Comment);
