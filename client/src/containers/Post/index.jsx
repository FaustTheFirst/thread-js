import React, { useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { Card, Image, Form } from 'semantic-ui-react';
import moment from 'moment';
import EditButtons from 'src/components/EditButtons';
import ReactionButtons from 'src/components/ReactionButtons';
import { reactPost, toggleExpandedPost, updatePost } from 'src/containers/Thread/postActions';

const Post = ({
  post,
  reactPost: setPostReaction,
  toggleExpandedPost: togglePost,
  updatePost: setPostUpdation,
  sharePost,
  deleteModal,
  activeUser
}) => {
  const {
    id,
    image,
    body,
    user,
    commentCount,
    createdAt,
    updatedAt,
    isEditable,
    postReactions
  } = post;

  const dateCreated = moment(createdAt).fromNow();
  const dateUpdated = createdAt !== updatedAt ? moment(updatedAt).fromNow() : null;
  const postHolder = activeUser === user.id;
  const [isEditPost, setIsEditPost] = useState(isEditable);
  const [postField, setPostField] = useState(body);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            {`posted by ${user.username} - ${dateCreated}`}
            {dateUpdated ? ` (last update - ${dateUpdated})` : null}
          </span>
        </Card.Meta>
        <Card.Description>
          {postHolder && isEditPost
            ? (
              <Form>
                <Form.TextArea
                  error={postField.length === 0}
                  value={postField}
                  onChange={ev => setPostField(ev.target.value)}
                />
              </Form>
            )
            : body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <ReactionButtons
          id={id}
          activeUser={activeUser}
          arrayOfReactions={postReactions}
          setReaction={setPostReaction}
          commentCount={commentCount}
          toggleExpandedPost={togglePost}
          sharePost={sharePost}
        />
        {postHolder
          ? (
            <EditButtons
              id={id}
              isEdit={isEditPost}
              setIsEdit={() => setIsEditPost(!isEditPost)}
              editField={postField}
              setEditField={() => setPostField(body)}
              setUpdation={setPostUpdation}
              deleteModal={deleteModal}
              modalCaller="post"
              toggleExpandedPost={togglePost}
            />
          )
          : null}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  activeUser: PropTypes.string.isRequired,
  reactPost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deleteModal: PropTypes.func
};

Post.defaultProps = {
  deleteModal: undefined
};

const actions = { reactPost, toggleExpandedPost, updatePost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  null,
  mapDispatchToProps
)(Post);
