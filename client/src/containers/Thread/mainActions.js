import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

export const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

export const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

export const addPostAction = post => ({
  type: ADD_POST,
  post
});

export const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});
