import * as postService from 'src/services/postService';
import * as actions from './mainActions';

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);

  dispatch(actions.setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const loadedPosts = await postService.getAllPosts(filter);
  const { posts: { posts } } = getRootState();
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));

  dispatch(actions.addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);

  dispatch(actions.addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);

  dispatch(actions.addPostAction(newPost));
};

export const toggleExpandedPost = (postId, isEditable) => async (dispatch, getRootState) => {
  const addEditFlag = post => ({
    ...post,
    isEditable
  });

  const { posts: { posts } } = getRootState();

  let temp;
  let updatePostOrPosts;

  if (postId) {
    temp = await postService.getPost(postId);
    if (temp.status) {
      updatePostOrPosts = posts.filter(post => post.id !== postId);
      dispatch(actions.setPostsAction(updatePostOrPosts));
    } else {
      updatePostOrPosts = addEditFlag(temp);
      dispatch(actions.setExpandedPostAction(updatePostOrPosts));
    }
  } else {
    dispatch(actions.setExpandedPostAction(undefined));
  }
};

export const updatePost = (postId, body) => async (dispatch, getRootState) => {
  const postUpdation = await postService.updatePost(postId, body);

  const mapUpdate = post => ({
    ...post,
    ...postUpdation
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id === postUpdation.id ? mapUpdate(post) : post));

  dispatch(actions.setPostsAction(updated));
  dispatch(actions.setExpandedPostAction(mapUpdate(expandedPost)));
};

export const deletePost = postId => async (dispatch, getRootState) => {
  const { deleteStatus } = await postService.deletePost(postId);
  const { posts: { posts } } = getRootState();
  const newListOfPosts = posts.filter(post => post.id !== postId);

  if (deleteStatus) {
    dispatch(actions.setExpandedPostAction(undefined));
    dispatch(actions.setPostsAction(newListOfPosts));
  }
};

export const reactPost = (postId, isLike) => async (dispatch, getRootState) => {
  const postReaction = await postService.reactPost(postId, isLike);

  const applyReaction = (arrayOfReactions, newReaction) => {
    let updatedReactions;
    if (arrayOfReactions.some(react => react.id === newReaction.id)) {
      updatedReactions = newReaction.deleted
        ? arrayOfReactions.filter(react => react.id !== newReaction.id)
        : arrayOfReactions.map(react => (react.id === newReaction.id ? newReaction : react));
    } else {
      updatedReactions = [newReaction, ...arrayOfReactions];
    }

    return updatedReactions;
  };

  const mapPosts = post => ({
    ...post,
    postReactions: applyReaction(post.postReactions, postReaction)
  });

  const { posts: { posts, expandedPost } } = getRootState();

  const updated = posts.map(post => (post.id === postId ? mapPosts(post) : post));

  dispatch(actions.setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(actions.setExpandedPostAction(mapPosts(expandedPost)));
  }
};
