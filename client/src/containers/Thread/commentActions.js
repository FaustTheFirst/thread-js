import * as commentService from 'src/services/commentService';
import * as actions from './mainActions';

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: `${Number(post.commentCount) + 1}`,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(actions.setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(actions.setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const updateComment = (commentId, body) => async (dispatch, getRootState) => {
  const commentUpdation = await commentService.updateComment(commentId, body);

  const mapUpdateComments = comment => ({
    ...comment,
    ...commentUpdation
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updatedComments = expandedPost.comments.map(comment => (
    comment.id === commentUpdation.id ? mapUpdateComments(comment) : comment));

  const mapUpdatePosts = post => ({
    ...post,
    comments: updatedComments
  });

  const updatedPosts = posts.map(post => (post.id === expandedPost.id ? mapUpdatePosts(post) : post));

  dispatch(actions.setPostsAction(updatedPosts));
  dispatch(actions.setExpandedPostAction(mapUpdatePosts(expandedPost)));
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  const { deleteStatus } = await commentService.deleteComment(commentId);
  const { posts: { posts, expandedPost } } = getRootState();
  const newListOfComments = expandedPost.comments.filter(comment => comment.id !== commentId);

  const mapPosts = post => ({
    ...post,
    commentCount: `${post.commentCount - 1}`,
    comments: newListOfComments
  });

  const updatedPosts = posts.map(post => (post.id === expandedPost.id ? mapPosts(post) : post));

  if (deleteStatus) {
    dispatch(actions.setPostsAction(updatedPosts));
    dispatch(actions.setExpandedPostAction(mapPosts(expandedPost)));
  }
};

export const reactComment = (commentId, isLike) => async (dispatch, getRootState) => {
  const commentReaction = await commentService.reactComment(commentId, isLike);

  const applyReaction = (arrayOfReactions, newReaction) => {
    let updatedReactions;

    if (arrayOfReactions.some(react => react.id === newReaction.id)) {
      updatedReactions = newReaction.deleted
        ? arrayOfReactions.filter(react => react.id !== newReaction.id)
        : arrayOfReactions.map(react => (react.id === newReaction.id ? newReaction : react));
    } else {
      updatedReactions = [newReaction, ...arrayOfReactions];
    }

    return updatedReactions;
  };

  const mapComments = comment => ({
    ...comment,
    commentReactions: applyReaction(comment.commentReactions, commentReaction)
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updatedComments = expandedPost.comments.map(comment => (
    commentId === comment.id ? mapComments(comment) : comment));

  const mapPosts = post => ({
    ...post,
    comments: updatedComments
  });

  const updatedPosts = posts.map(post => (post.id === expandedPost.id ? mapPosts(post) : post));

  dispatch(actions.setPostsAction(updatedPosts));
  dispatch(actions.setExpandedPostAction(mapPosts(expandedPost)));
};
