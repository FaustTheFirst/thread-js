/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import Post from 'src/containers/Post';
import AddPost from 'src/components/AddPost';
import SharedPostLink from 'src/components/SharedPostLink';
import { Loader, Menu, Icon, Button, Checkbox, List } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import { loadPosts, loadMorePosts, addPost } from './postActions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  showUserPosts: undefined,
  showLikedPosts: false,
  showDislikedPosts: false,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  addPost: createPost
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showPosts, setShowPosts] = useState('all');
  const [showFilter, setShowFilter] = useState(false);
  postsFilter.userId = userId;

  const toggleShowPosts = (e, { name }) => {
    switch (name) {
      case 'own':
      case 'other': {
        setShowPosts(name);
        postsFilter.showUserPosts = name === 'own';
        break;
      }
      case 'liked': {
        postsFilter.showLikedPosts = !postsFilter.showLikedPosts;
        break;
      }
      case 'disliked': {
        postsFilter.showDislikedPosts = !postsFilter.showDislikedPosts;
        break;
      }
      default: {
        setShowPosts(name);
        postsFilter.showUserPosts = undefined;
      }
    }

    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count;
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Menu pointing secondary color="blue" size="large">
          <Menu.Item
            name="all"
            active={showPosts === 'all'}
            onClick={toggleShowPosts}
          />
          <Menu.Item
            name="own"
            active={showPosts === 'own'}
            onClick={toggleShowPosts}
          />
          <Menu.Item
            name="other"
            active={showPosts === 'other'}
            onClick={toggleShowPosts}
          />
          <Menu.Item position="right">
            <Button
              icon
              labelPosition="left"
              color="blue"
              size="small"
              onClick={() => setShowFilter(!showFilter)}
            >
              <Icon name="filter" />
              Filter
            </Button>
          </Menu.Item>
        </Menu>
        {showFilter
          ? (
            <List>
              <List.Item>
                <Checkbox
                  toggle
                  name="liked"
                  label="Show liked posts"
                  checked={postsFilter.showLikedPosts}
                  onChange={toggleShowPosts}
                />
              </List.Item>
              <List.Item>
                <Checkbox
                  toggle
                  name="disliked"
                  label="Show disliked posts"
                  checked={postsFilter.showDislikedPosts}
                  onChange={toggleShowPosts}
                />
              </List.Item>
            </List>
          )
          : null}
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            sharePost={sharePost}
            key={post.id}
            activeUser={userId}
          />
        ))}
      </InfiniteScroll>
      {expandedPost && <ExpandedPost sharePost={sharePost} />}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  loadPosts,
  loadMorePosts,
  addPost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
