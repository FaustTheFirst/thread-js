import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import { toggleExpandedPost, deletePost } from 'src/containers/Thread/postActions';
import { addComment, reactComment, updateComment, deleteComment } from 'src/containers/Thread/commentActions';
import Post from 'src/containers/Post';
import Comment from 'src/containers/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';
import DeleteModal from 'src/components/DeleteModal';

const ExpandedPost = ({
  post,
  sharePost,
  toggleExpandedPost: toggle,
  addComment: add,
  deletePost: postDelete,
  deleteComment: commentDelete,
  userId
}) => {
  const [showDeleteModal, setShowDeleteModal] = useState({ id: '', modalCaller: false });

  const deleteModal = (id, modalCaller) => {
    setShowDeleteModal({ id, modalCaller });
  };

  let deleteMethod;
  if (showDeleteModal.modalCaller === 'post') {
    deleteMethod = postDelete;
  } else if (showDeleteModal.modalCaller === 'comment') {
    deleteMethod = commentDelete;
  }

  return (
    <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              activeUser={userId}
              sharePost={sharePost}
              deleteModal={deleteModal}
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    key={comment.id}
                    comment={comment}
                    activeUser={userId}
                    deleteModal={deleteModal}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} />
            </CommentUI.Group>
            {showDeleteModal.modalCaller
              ? (
                <DeleteModal
                  modalInfo={showDeleteModal}
                  close={() => setShowDeleteModal(false)}
                  deleteMethod={deleteMethod}
                />
              )
              : null}
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  userId: PropTypes.string
};

ExpandedPost.defaultProps = {
  userId: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  toggleExpandedPost,
  addComment,
  deletePost,
  updateComment,
  deleteComment,
  reactComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
