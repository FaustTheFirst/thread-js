import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button, Input, Label } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Header = ({ user, logout, updateStatus }) => {
  const [isEditStatus, setIsEditStatus] = useState(false);
  const [statusField, setStatusField] = useState(user.userStatus);
  const inputRef = useRef(null);

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <HeaderUI>
              <NavLink exact to="/">
                <Image circular className={styles.userImage} src={getUserImgLink(user.image)} size="large" />
              </NavLink>
              <HeaderUI.Content>
                {user.username}
                <HeaderUI.Subheader>
                  <Input
                    ref={inputRef}
                    className={styles.statusInput}
                    placeholder="(Your status)"
                    value={statusField || ''}
                    transparent={!isEditStatus}
                    onFocus={() => setIsEditStatus(true)}
                    onChange={ev => {
                      if (ev.target.value.length < 15) {
                        setStatusField(ev.target.value);
                      }
                    }}
                  />
                  {!isEditStatus
                    ? (
                      <Label
                        basic
                        key="edit"
                        className={styles.statusEditBtn}
                        as="a"
                        onClick={() => {
                          setIsEditStatus(!isEditStatus);
                          inputRef.current.focus();
                        }}
                      >
                        <Icon name="pencil" />
                      </Label>
                    )
                    : (
                      <>
                        <Label
                          basic
                          key="done"
                          className={styles.statusEditBtn}
                          as="a"
                          onClick={() => {
                            updateStatus(statusField);
                            setIsEditStatus(!isEditStatus);
                          }}
                        >
                          <Icon name="check" />
                        </Label>
                        <Label
                          basic
                          key="cancel"
                          className={styles.statusEditBtn}
                          as="a"
                          onClick={() => {
                            setIsEditStatus(!isEditStatus);
                            setStatusField(user.userStatus);
                          }}
                        >
                          <Icon name="ban" />
                        </Label>
                      </>
                    )}
                </HeaderUI.Subheader>
              </HeaderUI.Content>
            </HeaderUI>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  updateStatus: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired
};

export default Header;
