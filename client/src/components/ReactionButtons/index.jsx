import React from 'react';
import PropTypes from 'prop-types';
import { Label, Icon, Popup, Image } from 'semantic-ui-react';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const ReactionButtons = ({
  id,
  activeUser,
  arrayOfReactions,
  setReaction,
  commentCount,
  toggleExpandedPost,
  sharePost
}) => {
  const arrayOfLikes = arrayOfReactions.filter(reaction => reaction.isLike === true);
  const arrayOfDislikes = arrayOfReactions.filter(reaction => reaction.isLike === false);

  const isActiveUserLiked = arrayOfLikes.some(reaction => reaction.user.id === activeUser);
  const isActiveUserDisliked = arrayOfDislikes.some(reaction => reaction.user.id === activeUser);

  const showRecentReactions = array => (
    <>
      {array
        .filter((reaction, index) => index < 5)
        .map(reaction => <Image key={reaction.id} avatar src={getUserImgLink(reaction.user.image)} />)}
      {array.length > 5
        ? `...and ${array.length - 5} more`
        : null}
    </>
  );

  return (
    <>
      <Popup
        header={`Like ${arrayOfLikes.length} people`}
        content={showRecentReactions(arrayOfLikes)}
        inverted
        flowing
        on="hover"
        trigger={(
          <Label
            as="a"
            basic
            size="small"
            color={isActiveUserLiked ? 'green' : null}
            className={`${styles.toolbarBtn} ${styles.like}`}
            onClick={() => setReaction(id, true)}
          >
            <Icon name="thumbs up" />
            {arrayOfLikes.length}
          </Label>
        )}
      />
      <Popup
        header={`Dislike ${arrayOfDislikes.length} people`}
        content={showRecentReactions(arrayOfDislikes)}
        inverted
        flowing
        on="hover"
        trigger={(
          <Label
            as="a"
            basic
            size="small"
            color={isActiveUserDisliked ? 'red' : null}
            className={`${styles.toolbarBtn} ${styles.dislike}`}
            onClick={() => setReaction(id, false)}
          >
            <Icon name="thumbs down" />
            {arrayOfDislikes.length}
          </Label>
        )}
      />
      {sharePost && toggleExpandedPost && commentCount
        ? (
          <>
            <Label
              as="a"
              basic
              size="small"
              className={`${styles.toolbarBtn} ${styles.comment}`}
              onClick={() => toggleExpandedPost(id, false)}
            >
              <Icon name="comment" />
              {commentCount}
            </Label>
            <Label
              as="a"
              basic
              size="small"
              className={`${styles.toolbarBtn} ${styles.share}`}
              onClick={() => sharePost(id)}
            >
              <Icon name="share alternate" />
            </Label>
          </>
        )
        : null}
    </>
  );
};

ReactionButtons.propTypes = {
  id: PropTypes.string.isRequired,
  activeUser: PropTypes.string.isRequired,
  arrayOfReactions: PropTypes.arrayOf(PropTypes.any).isRequired,
  setReaction: PropTypes.func.isRequired,
  commentCount: PropTypes.string,
  toggleExpandedPost: PropTypes.func,
  sharePost: PropTypes.func
};

ReactionButtons.defaultProps = {
  commentCount: undefined,
  toggleExpandedPost: undefined,
  sharePost: undefined
};

export default ReactionButtons;
