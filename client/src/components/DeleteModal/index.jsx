import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Icon, Button, Header } from 'semantic-ui-react';

const DeleteModal = ({ modalInfo: { id, modalCaller }, close, deleteMethod }) => (
  <Modal open size="small" onClose={close}>
    <Header icon="exclamation triangle" content="Confirm delete" />
    <Modal.Content>
      {`Do you really want delete this ${modalCaller}? This process can't be undone`}
    </Modal.Content>
    <Modal.Actions>
      <Button color="red" inverted onClick={close}>
        <Icon name="remove" />
        No
      </Button>
      <Button
        color="green"
        inverted
        onClick={() => {
          deleteMethod(id);
          close();
        }}
      >
        <Icon name="checkmark" />
        Yes
      </Button>
    </Modal.Actions>
  </Modal>
);

DeleteModal.propTypes = {
  modalInfo: PropTypes.objectOf(PropTypes.any).isRequired,
  close: PropTypes.func.isRequired,
  deleteMethod: PropTypes.func.isRequired
};

export default DeleteModal;
