import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon } from 'semantic-ui-react';

const EditButtons = ({
  id,
  isEdit,
  setIsEdit,
  editField,
  setEditField,
  setUpdation,
  deleteModal,
  modalCaller,
  toggleExpandedPost
}) => (
  <>
    {isEdit
      ? (
        <>
          <Button
            icon
            as="label"
            color="red"
            floated="right"
            size="small"
            key="delete"
            onClick={() => deleteModal(id, modalCaller)}
          >
            <Icon name="trash" />
          </Button>
          <Button
            icon
            as="label"
            color="yellow"
            floated="right"
            size="small"
            key="cancel"
            onClick={() => {
              setIsEdit();
              setEditField();
            }}
          >
            <Icon name="ban" />
          </Button>
          <Button
            icon
            as="label"
            color="green"
            floated="right"
            size="small"
            disabled={editField.length === 0}
            key="done"
            onClick={() => {
              setIsEdit();
              setUpdation(id, editField);
            }}
          >
            <Icon name="check" />
          </Button>
        </>
      )
      : (
        <Button
          icon
          labelPosition="left"
          as="label"
          color="blue"
          floated="right"
          key="edit"
          onClick={() => {
            if (isEdit === undefined) {
              toggleExpandedPost(id, true);
            } else {
              setIsEdit();
            }
          }}
        >
          <Icon name="edit outline" />
          Edit
        </Button>
      )}
  </>
);

EditButtons.propTypes = {
  id: PropTypes.string.isRequired,
  editField: PropTypes.string.isRequired,
  isEdit: PropTypes.bool,
  setUpdation: PropTypes.func,
  deleteModal: PropTypes.func,
  setEditField: PropTypes.func.isRequired,
  setIsEdit: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func,
  modalCaller: PropTypes.string.isRequired
};

EditButtons.defaultProps = {
  isEdit: undefined,
  setUpdation: undefined,
  toggleExpandedPost: undefined,
  deleteModal: undefined
};

export default EditButtons;
